import java.util.Scanner;

/**
 * Составить программу, которая в зависимости от порядкового номера дня месяца  (1, 2, ...,  12)
 * выводит на  экран  время  года,  к  которому  относится  этот месяц.
 *
 */

public class TaskCh04N106 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int month = scanner.nextInt();
        String season = getSeason(month);
        System.out.println(season);
    }

    private static String getSeason(int month) {
        String season;
        switch (month) {
            case 12:
            case 1:
            case 2:
                season = "Winter";
                break;
            case 3:
            case 4:
            case 5:
                season = "Spring";
                break;
            case 6:
            case 7:
            case 8:
                season = "Summer";
                break;
            case 9:
            case 10:
            case 11:
                season = "Autumn";
                break;
            default:
                season = "Unknown season";
        }
        return season;
    }

}
