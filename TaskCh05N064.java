import java.util.Scanner;

/**
 * В  области  12  районов.  Известны  количество  жителей  (в  тысячах  человек)
 * и площадь (в км2 ) каждого района. Определить среднюю плотность населения
 * по области в целом.
 *
 */

public class TaskCh05N064 {

    public static void main(String[] args) {
        int numOfPeople = Integer.parseInt(args[0]);
        double areaTotal = 0;
        for (int i = 1; i < args.length; ++i) {
            areaTotal += Double.parseDouble(args[i]);
        }
        double avrgDensity = numOfPeople / areaTotal;
        System.out.printf("Средняя плотность: %.3f тыс. человек на 1 км2\n", avrgDensity);
    }

}
