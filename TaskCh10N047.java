/**
 * Написать рекурсивную функцию для вычисления k-го члена последовательности Фибоначчи.
 * Последовательность Фибоначчи f1, f2 , ... образуется по закону:
 * f1 = 1; f2 = 1; fi = fi-1 + fi-2, (i=3, 4, ...)
 *
 */

import java.math.BigInteger;
import java.util.Scanner;

public class TaskCh10N047 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        BigInteger[] accum = {BigInteger.valueOf(1), BigInteger.valueOf(1)};
        BigInteger nFib = fibbonacci(accum, n);
        System.out.println(nFib);
    }

    private static BigInteger fibbonacci(BigInteger[] accum, int n) {
        if (n <= 2) {
            return accum[1];
        }
        BigInteger tmp = accum[1];
        accum[1] = accum[1].add(accum[0]);
        accum[0] = tmp;
        return fibbonacci(accum, n - 1);
    }
}
