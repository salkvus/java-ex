/**
 * Написать рекурсивную функцию нахождения цифрового корня натурального числа. Цифровой корень данного
 * числа получается следующим образом. Если сложить все цифры этого числа, затем все цифры найденной
 * суммы  и повторять этот процесс, то в результате будет получено однозначное число (цифра), которая
 * и называется цифровым корнем данного числа.
 *
 */

import java.util.Scanner;

public class TaskCh10N044 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        int root = digitRoot(sumOfDigits(n));
        System.out.println(root);
    }

    private static int digitRoot(int n) {
        if (n / 10 == 0) {
            return  n;
        }
        return digitRoot(sumOfDigits(n));
    }

    private static int sumOfDigits(long n) {
        if (n == 0) {
            return 0;
        }
        return (int) (n % 10 + sumOfDigits(n / 10));
    }

}
