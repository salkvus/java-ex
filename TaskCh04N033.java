/**
 * Дано натуральное число.
 * а) Верно ли, что оно заканчивается четной цифрой?
 * б) Верно ли, что оно заканчивается нечетной цифрой?
 *
 */

import java.util.Scanner;

public class TaskCh04N033 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.printf("Число %d заканчивается %s цифрой", n, n % 10 % 2 == 0 ? "четной" : "нечетной");
    }
}
