import java.util.Scanner;

/**
 * Дано число n. Из чисел 1, 4, 9, 16, 25, ... напечатать те, которые не превышают n.
 *
 */

public class TaskCh06N008 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 1; i * i <= n; ++i) {
            System.out.println(i * i);
        }
    }

}
