/**
 * Дано слово. Вывести на экран его k-й символ
 *
 */

import java.util.Scanner;

public class TaskCh09N015 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        int n = scanner.nextInt();
        if (n > word.length()) {
            System.out.printf("Error: wrong character position. It must be equal or lower than %d\n", word.length());
            return;
        }
        System.out.printf("%d character is '%c'", n, word.charAt(n - 1));
    }

}
