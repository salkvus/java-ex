/**
 * Написать рекурсивную функцию для вычисления максимального элемента массива из n элементов.
 *
 */

import java.util.Scanner;

public class TaskCh10N048 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; ++i) {
            array[i] = scanner.nextInt();
        }
        int max = getMax(array, array[n - 1], n - 2);
        System.out.println(max);
    }

    private static int getMax(int[] array, int max, int n) {
        if (n == 0) {
            return Math.max(max, array[n]);
        };
        max = Math.max(max, array[n]);
        return getMax(array, max, n - 1);
    }
}
