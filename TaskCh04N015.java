/**
 * Известны год и номер месяца рождения человека, а также год и номер месяца
 * сегодняшнего  дня  (январь —  1  и  т. д.).  Определить  возраст  человека  (число
 * полных  лет).  В  случае  совпадения  указанных  номеров  месяцев  считать,  что
 * прошел полный год.
 *
 */

import java.util.Scanner;

public class TaskCh04N015 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int nowMonth = scanner.nextInt();
        int nowYear = scanner.nextInt();
        int birthMonth = scanner.nextInt();
        int birthYear = scanner.nextInt();
        int years = calcYears(nowMonth, nowYear, birthMonth, birthYear);
        System.out.println(years);
    }

    private static int calcYears(int nowMonth, int nowYear, int birthMonth, int birthYear) {
        return nowYear - birthYear - (nowMonth < birthMonth ? 1 : 0);
    }
}
