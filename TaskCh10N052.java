/**
 * Написать рекурсивную процедуру для вывода на экран цифр натурального числа в обратном порядке.
 *
 */

import java.util.Scanner;

public class TaskCh10N052 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        System.out.println(getReverseNumber(n, 0));
    }

    private static long getReverseNumber(long n, long accum) {
        accum = accum * 10 + n % 10;
        if (n / 10 == 0) {
            return accum;
        }
        return getReverseNumber(n / 10, accum);
    }

}
