/**
 * Дано слово, состоящее из четного числа букв. Вывести на экран его первую половину, не используя
 * оператор цикла.
 *
 */

import java.util.Scanner;

public class TaskCh09N022 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.next().trim();
        String partWord = word.substring(0, word.length() / 2);
        System.out.println(partWord);
    }
}
