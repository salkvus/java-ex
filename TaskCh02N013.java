/**
 * Дано трехзначное число. Найти число, полученное при прочтении его цифр справа налево.
 *
 */

import java.util.Scanner;

public class TaskCh02N013 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = reverseNumber(n, 3);
        System.out.println(m);
    }

    private static int reverseNumber(int n, int maxDegree) {
        int result = 0;
        for (int i = 1; i <= maxDegree; ++i) {
            int dig = n % 10;
            result += dig * (int)Math.pow(10, maxDegree - i);
            n /= 10;
        }
        return  result;
    }

}
