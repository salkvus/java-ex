/**
 * Дано слово. Верно ли, что оно начинается и оканчивается на одну и ту же  букву?
 *
 */

import java.util.Scanner;

public class TaskCh09N017 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.next().trim();
        boolean isSameChar = Character.toLowerCase(word.charAt(0)) == Character.toLowerCase(word.charAt(word.length() - 1));
        String answer = String.format("Word '%s' is %s begining and ending with the same character" , word, isSameChar ? "" : " not ");
        System.out.println(answer);
    }

}
