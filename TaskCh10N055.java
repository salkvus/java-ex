/**
 *  Написать рекурсивную процедуру перевода натурального числа из десятичной системы счисления в N-ричную.
 *  Значение N в основной программе вводится с клавиатуры (2 <= N <= 16).
 *
 */

import java.util.Scanner;

public class TaskCh10N055 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int radix = scanner.nextInt();
        String mToRadix = decimalToRadix(n, radix);
        System.out.println(mToRadix);
    }

    private  static String decimalToRadix(int n, int radix) {
        return helperToRadix(n, radix, "");
    }

    private static String helperToRadix(int n, int radix, String accum) {
        int digit = n % radix;
        String sDigit = String.valueOf(digit);
        if (digit > 9) {
            sDigit = Character.toString('A' + digit - 10);
        }
        accum = sDigit + accum;
        if (n / radix == 0) {
            return accum;
        }
        return helperToRadix(n / radix, radix, accum);
    }

}
