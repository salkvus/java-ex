/**
 *  Составить программу, которая печатает заданное слово, начиная с последней буквы.
 *
 */

import java.util.Scanner;

public class TaskCh09N042 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder word = new StringBuilder(scanner.next().trim());
        for (int i = 0; i < word.length() / 2; ++i) {
            char tmpChar = word.charAt(i);
            int index = word.length() - i - 1;
            word.setCharAt(i, word.charAt(index));
            word.setCharAt(index, tmpChar);
        }
        System.out.println(word);
    }
}
