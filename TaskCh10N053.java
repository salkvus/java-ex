/**
 * Написать рекурсивную процедуру для ввода с клавиатуры последовательности чисел и вывода ее на экран
 * в обратном порядке (окончание последовательности — при вводе нуля).
 *
 */

import java.util.Scanner;

public class TaskCh10N053 {

    public static void main(String[] args) {
        String[] inputArray = inputDigits(";").split(";");
        outputDigits(inputArray, inputArray.length - 1);
    }

    private static String inputDigits(String delimiter) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder input = new StringBuilder();
        return inputHelper(scanner, input, delimiter).toString();
    }

    private static StringBuilder inputHelper(Scanner scanner, StringBuilder input, String delimiter) {
        if(scanner.hasNext("0")) {
            return input;
        }
        input.append(scanner.next()).append(delimiter);
        return inputHelper(scanner, input, delimiter);
    }

    private static void outputDigits(String[] array, int index) {
        if (index < 0) {
            return;
        };
        System.out.printf("%s ", array[index]);
        outputDigits(array, index - 1);
    }

}
