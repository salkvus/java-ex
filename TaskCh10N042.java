/**
 * В некоторых языках программирования (например, в Паскале) не предусмотрена операция возведения
 * в степень. Написать рекурсивную функцию для расчета степени n вещественного числа a
 * (n — натуральное число).
 *
 */

import java.util.Scanner;

public class TaskCh10N042 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        int n = scanner.nextInt();
        a = powDouble(a, n);
        System.out.println(a);
    }

    private static double powDouble(double a, int n) {
        if (n == 1) {
            return  a;
        }
        return a * powDouble(a, n - 1);
    }
}
