/**
 * Дано слово. Поменять местами первую из букв а и последнюю из букв о. Учесть возможность того,
 * что таких букв в слове может не быть.
 *
 */

import java.util.Scanner;

public class TaskCh09N107 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder word = new StringBuilder(scanner.next().trim());
        int indexA = getSubstrIndex(word.toString(), "а", true);
        int indexO = getSubstrIndex(word.toString(), "о", false);
        if (indexA >= 0 && indexO >= 0) {
            char a = word.charAt(indexA);
            word.setCharAt(indexA, word.charAt(indexO));
            word.setCharAt(indexO, a);
            System.out.println(word.toString());
        } else {
            System.out.println("Word wasn't changed");
        }
    }

    private static int getSubstrIndex(String word, String subStr, boolean firstIndex) {
        String lowerSubstr = subStr.toLowerCase();
        int index1 = firstIndex ? word.indexOf(lowerSubstr) : word.lastIndexOf(lowerSubstr);
        String upperSubstr = subStr.toUpperCase();
        int index2 = firstIndex ? word.indexOf(upperSubstr) : word.lastIndexOf(upperSubstr);
        int indexStr = -1;
        if (firstIndex) {
            indexStr = index1 >= 0 && index2 >= 0 ? Math.min(index1, index2) : Math.max(index1, index2);
        } else {
            indexStr = index1 >= 0 && index2 >= 0 ? Math.max(index1, index2) : Math.max(index1, index2);
        }
        return indexStr;
    }
}
