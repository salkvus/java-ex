/**
 * Дано предложение. Поменять местами его первое и последнее слово.
 *
 */

import java.util.Scanner;

public class TaskCh09N166 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] words = scanner.nextLine().split(" ");
        String first = words[0];
        words[0] = words[words.length - 1];
        words[words.length - 1] = first;
        String changed = String.join(" ", words);
        System.out.println(changed);
    }
}
