/**
 * Работа светофора для пешеходов запрограммирована следующим образом: в
 * начале каждого часа в течение трех минут горит зеленый сигнал, затем в те-
 * чение двух минут — красный, в течение трех минут — опять зеленый и т. д.
 * Дано вещественное число t, означающее время в минутах, прошедшее с нача-
 * ла очередного часа. Определить, сигнал какого цвета горит для пешеходов в
 * этот момент.
 *
 */

import java.util.Scanner;

public class TaskCh04N036 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        System.out.println(getTraficLightColor(t));
    }

    private static String getTraficLightColor(int t) {
        int greenTime = 3;
        int redTime = 2;
        if (t % (greenTime + redTime) < greenTime) {
            return "green";
        } else {
            return "red";
        }
    }

}
