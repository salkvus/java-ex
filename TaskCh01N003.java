/**
 * Составить программу вывода на экран числа, вводимого с клавиатуры.
 * Выводимому числу должно предшествовать сообщение "Вы ввели число".
 *
 */

import java.util.Scanner;

class TaskCh01N003 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        System.out.printf("Вы ввели число %d", a);
    }
}
