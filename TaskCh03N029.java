/**
 * notes:
 * don't use *"/"+- operators, in b solve with <=2 and in d - <=3 comparison operators;
 *
 * а) каждое из чисел X и Y нечетное;
 * б) только одно из чисел X и Y меньше 20;
 * в) хотя бы одно из чисел X и Y равно нулю;
 * г) каждое из чисел X, Y, Z отрицательное;
 * д) только одно из чисел X, Y и Z кратно пяти;
 * е) хотя бы одно из чисел X, Y, Z  больше 100.
 */

public class TaskCh03N029 {

    public static void main(String[] args) {
        int x = 1, y = 3, z = -3;
        boolean resA = x % 2 == 1 && y % 2 == 1;
        System.out.printf("каждое из чисел %d и %d нечетное: %b\n", x, y, resA);

        x = 19; y = 20;
        boolean resB = !(x / 10 < 2) ^ !(y / 10 < 2); // как использовать <=2 не догадался
        System.out.printf("только одно из чисел %d и %d меньше 20: %b\n", x, y, resB);

        x = 1; y = 0;
        boolean resV = x == 0 || y == 0;
        System.out.printf("хотя бы одно из чисел %d и %d равно нулю: %b\n", x, y, resV);

        x = -1; y = -2; z = -3;
        boolean resG = x < 0 && y < 0 && z < 0;
        System.out.printf("каждое из чисел %d, %d, %d отрицательное: %b\n", x, y, z, resG);

        x = 20; y = 30; z = 36;
        boolean resD = x % 5 == 0 ^ y % 5 == 0 ^ z % 5 == 0; // как использовать <= 3 не догадался
        System.out.printf("только одно из чисел %d, %d и %d кратно пяти: %b\n", x, y, z, resD);

        x = 50; y = 101; z = 30;
        boolean resE = x > 100 || y > 100 || z > 100;
        System.out.printf("хотя бы одно из чисел %d, %d, %d  больше 100: %b\n", x, y, z, resE);
    }
}
