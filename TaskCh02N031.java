/**
 *  В трехзначном числе x зачеркнули его вторую цифру. Когда к образованному при этом двузначному числу справа
 *  приписали вторую цифру числа x, то получилось число n.
 *  По заданному n найти число x (значение n вводится с клавиатуры, 100 ≤ n ≤ 999).
 *
 */

import java.util.Scanner;

public class TaskCh02N031 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x = getNumber(n);
        System.out.println(x);
    }

    private static int getNumber(int n) {
        int x2 = n % 10;
        int x1 = n % 100 / 10;
        return  n / 100 * 100 + x2 * 10 + x1;
    }

}
