/**
 * Дан двумерный массив.
 * а) Удалить из него k-ю строку.
 * б) Удалить из него s-й столбец.
 *
 */

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh12N234 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int s = scanner.nextInt();

        int[][] arrayA = createArray(7, 10);
        printArray(arrayA);
        System.out.println();
        deleteRowFromArray(arrayA, k - 1);
        printArray(arrayA);
        int[][] arrayB = createArray(7, 10);
        System.out.println();
        deleteColumnFromArray(arrayB, s - 1);
        printArray(arrayB);
    }

    private static void deleteRowFromArray(int[][] array, int index) {
        int[] delRow = array[index];
        for (int i = 0; i < array.length - index - 1; ++i) {
            array[index + i] = array[index + i + 1];
        }
        array[array.length - 1] = delRow;
        Arrays.fill(array[array.length - 1], 0, array[array.length - 1].length, 0);
    }

    private static void deleteColumnFromArray(int[][] array, int index) {
        for (var row : array) {
            if (index != row.length - 1) {
                System.arraycopy(row, index + 1, row, index, row.length - index - 1);
            }
            row[row.length - 1] = 0;
        }
    }

   private static int[][] createArray(int n, int m) {
        int[][] array = new int[n][m];
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                array[i][j] = j + 1 + i * m;
            }
        }
        return array;
    }

    private static void printArray(int[][] array) {
        for (var row : array) {
            System.out.println(Arrays.toString(row).replaceAll("[\\[\\],]", ""));
        }
    }

}
