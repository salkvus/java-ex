/**
 *  Написать рекурсивную функцию:
 *  а) вычисления суммы цифр натурального числа;
 *  б) вычисления количества цифр натурального числа.
 *
 */

import java.util.Scanner;

public class TaskCh10N043 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        int sumDigs = sumOfDigits(n);
        int quantDigs = quantityOfDigits(n);
        System.out.printf("%d - sum of digits equals %d, quantity of digits equals %d", n, sumDigs, quantDigs);
    }

    private static int sumOfDigits(long n) {
        if (n == 0) {
            return 0;
        }
        return (int) (n % 10 + sumOfDigits(n / 10));
    }

    private static int quantityOfDigits(long n) {
        if (n == 0) {
            return 0;
        }
        return 1 + quantityOfDigits(n / 10);
    }
}
