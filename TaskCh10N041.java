/**
 * Написать рекурсивную функцию для вычисления факториала натурального числа n.
 *
 */

import java.math.BigInteger;
import java.util.Scanner;

public class TaskCh10N041 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        BigInteger nFact = factorial(n);
        System.out.println(nFact);
    }

    private static BigInteger factorial(int n) {
        if (n == 0 || n == 1) {
            return BigInteger.valueOf(1);
        }
        return BigInteger.valueOf(n).multiply(factorial(n - 1));
    }

}
