/**
 * Заполнить двумерный массив так, как представлено на рис. 12.3
 *
 * а) 1 2 3 4 5 6 7 8 9 10
 *    11 12 13 14 15 16 17 18 19 20
 *    21 22 23 24 25 26 27 28 29 30
 *    31 32 33 34 35 36 37 38 39 40
 *    41 42 43 44 45 46 47 48 49 50
 *    51 52 53 54 55 56 57 58 59 60
 *    61 62 63 64 65 66 67 68 69 70
 *    71 72 73 74 75 76 77 78 79 80
 *    81 82 83 84 85 86 87 88 89 90
 *    91 92 93 94 95 96 97 98 99 100
 *    101 102 103 104 105 106 107 108 109 110
 *    111 112 113 114 115 116 117 118 119 120
 *
 * б) 1 13 25 37 49 61 73 85 97 109
 *    2 14 26 38 50 62 74 86 98 110
 *    3 15 27 39 51 63 75 87 99 111
 *    4 16 28 40 52 64 76 88 100 112
 *    5 17 29 41 53 65 77 89 101 113
 *    6 18 30 42 54 66 78 90 102 114
 *    7 19 31 43 55 67 79 91 103 115
 *    8 20 32 44 56 68 80 92 104 116
 *    9 21 33 45 57 69 81 93 105 117
 *    10 22 34 46 58 70 82 94 106 118
 *    11 23 35 47 59 71 83 95 107 119
 *    12 24 36 48 60 72 84 96 108 120
 *
 * в) 10 9 8 7 6 5 4 3 2 1
 *    20 19 18 17 16 15 14 13 12 11
 *    30 29 28 27 26 25 24 23 22 21
 *    40 39 38 37 36 35 34 33 32 31
 *    50 49 48 47 46 45 44 43 42 41
 *    60 59 58 57 56 55 54 53 52 51
 *    70 69 68 67 66 65 64 63 62 61
 *    80 79 78 77 76 75 74 73 72 71
 *    90 89 88 87 86 85 84 83 82 81
 *    100 99 98 97 96 95 94 93 92 91
 *    110 109 108 107 106 105 104 103 102 101
 *    120 119 118 117 116 115 114 113 112 111
 *
 * г) 12 24 36 48 60 72 84 96 108 120
 *    11 23 35 47 59 71 83 95 107 119
 *    10 22 34 46 58 70 82 94 106 118
 *    9 21 33 45 57 69 81 93 105 117
 *    8 20 32 44 56 68 80 92 104 116
 *    7 19 31 43 55 67 79 91 103 115
 *    6 18 30 42 54 66 78 90 102 114
 *    5 17 29 41 53 65 77 89 101 113
 *    4 16 28 40 52 64 76 88 100 112
 *    3 15 27 39 51 63 75 87 99 111
 *    2 14 26 38 50 62 74 86 98 110
 *    1 13 25 37 49 61 73 85 97 109
 *
 * д) 1 2 3 4 5 6 7 8 9 10 11 12
 *    24 23 22 21 20 19 18 17 16 15 14 13
 *    25 26 27 28 29 30 31 32 33 34 35 36
 *    48 47 46 45 44 43 42 41 40 39 38 37
 *    49 50 51 52 53 54 55 56 57 58 59 60
 *    72 71 70 69 68 67 66 65 64 63 62 61
 *    73 74 75 76 77 78 79 80 81 82 83 84
 *    96 95 94 93 92 91 90 89 88 87 86 85
 *    97 98 99 100 101 102 103 104 105 106 107 108
 *    120 119 118 117 116 115 114 113 112 111 110 109
 *
 * е) 1 24 25 48 49 72 73 96 97 120
 *    2 23 26 47 50 71 74 95 98 119
 *    3 22 27 46 51 70 75 94 99 118
 *    4 21 28 45 52 69 76 93 100 117
 *    5 20 29 44 53 68 77 92 101 116
 *    6 19 30 43 54 67 78 91 102 115
 *    7 18 31 42 55 66 79 90 103 114
 *    8 17 32 41 56 65 80 89 104 113
 *    9 16 33 40 57 64 81 88 105 112
 *    10 15 34 39 58 63 82 87 106 111
 *    11 14 35 38 59 62 83 86 107 110
 *    12 13 36 37 60 61 84 85 108 109
 *
 */

import java.util.Arrays;

public class TaskCh12N025 {

    public static void main(String[] args) {

        int[][] arrayA = createArrayA(12, 10);
        printArray(arrayA);
        System.out.println();
        int[][] arrayB = createArrayB(12, 10);
        printArray(arrayB);
        System.out.println();
        int[][] arrayV = createArrayV(12, 10);
        printArray(arrayV);
        System.out.println();
        int[][] arrayG = createArrayG(12, 10);
        printArray(arrayG);
        System.out.println();
        int[][] arrayD = createArrayD(10, 12);
        printArray(arrayD);
        System.out.println();
        int[][] arrayE = createArrayE(12, 10);
        printArray(arrayE);
    }

    private static int[][] createArrayA(int n, int m) {
        int[][] array = new int[n][m];
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                array[i][j] = j + 1 + i * m;
            }
        }
        return array;
    }

    private static int[][] createArrayB(int n, int m) {
        int[][] array = new int[n][m];
        for (int j = 0; j < m; ++j) {
             for (int i = 0; i < n; ++i) {
                array[i][j] = (i + 1) + j * n;
            }
        }
        return array;
    }

    private static int[][] createArrayV(int n, int m) {
        int[][] array = new int[n][m];
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                array[i][m - 1 - j] = (j + 1) + i * m;
            }
        }
        return array;
    }

    private static int[][] createArrayG(int n, int m) {
        int[][] array = new int[n][m];
        for (int j = 0; j < m; ++j) {
            for (int i = 0; i < n; ++i) {
                array[n - 1 - i][j] = (i + 1) + j * n;
            }
        }
        return array;
    }

    private static int[][] createArrayD(int n, int m) {
        int[][] array = new int[n][m];
        for (int i = 0; i < n; ++i) {
            boolean isOdd = (i + 1) % 2 == 1;
            int value = 0;
            for (int j = 0; j < m; ++j) {
                value = (j + 1) + i * m;
                if (isOdd) {
                    array[i][j] = value;
                } else {
                    array[i][m - 1 - j] = value;
                }
            }
        }
        return array;
    }

    private static int[][] createArrayE(int n, int m) {
        int[][] array = new int[n][m];
        for (int j = 0; j < m; ++j) {
            boolean isOdd = (j + 1) % 2 == 1;
            int value = 0;
            for (int i = 0; i < n; ++i) {
                value = (i + 1) + j * n;
                if (isOdd) {
                    array[i][j] = value;
                } else {
                    array[n - 1 - i][j] = value;
                }
            }
        }
        return array;
    }

    private static void printArray(int[][] array) {
        for (var row : array) {
            System.out.println(Arrays.toString(row).replaceAll("[\\[\\],]", ""));
        }
    }

}

