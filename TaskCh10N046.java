/**
 * Даны первый член и знаменатель геометрической прогрессии. Написать рекурсивную функцию:
 * а) нахождения n-го члена прогрессии;
 * б) нахождения суммы n первых членов прогрессии.
 *
 */

import java.util.Scanner;

public class TaskCh10N046 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double b1 = scanner.nextDouble();
        double q = scanner.nextDouble();
        long n = scanner.nextLong();
        double bN = getNthMember(b1, q, n);
        double sum = getProgressionSum(b1, q, n);
        System.out.println(bN);
        System.out.println(sum);
    }

    private static double getNthMember(double b1, double q, long n) {
        if (n == 1) {
            return b1;
        }
        return q * getNthMember(b1, q, n - 1);
    }

    private static double getProgressionSum(double b, double q, long n) {
        if (n == 0) {
            return 0;
        }
        return b + getProgressionSum(b * q, q, n - 1);
    }
}
