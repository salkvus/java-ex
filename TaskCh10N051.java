/**
 * Определить результат выполнения следующих рекурсивных процедур при n = 5 :
 * а) алг Процедура1(арг цел n)
 *    нач
 *      если n > 0 то
 *         вывод n
 *         Процедура1(n – 1)
 *      все
 *    кон
 * б) алг Процедура2(арг цел n)
 *    нач
 *      если n > 0 т
 *         Процедура2(n – 1)
 *         вывод n
 *      все
 *    кон
 *
 * в) алг Процедура3(арг цел n)
 *    нач
 *      если n > 0 то
 *         вывод n
 *         Процедура3(n – 1)
 *         вывод n
 *      все
 *    кон
 *
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TaskCh10N051 {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        File file = new File("./ActualOutputs10N051.txt");
        FileWriter writer = new FileWriter(file);

        outputString("а)--", writer);
        procedure1(n, writer);
        outputString("б)--", writer);
        procedure2(n, writer);
        outputString("в)--", writer);
        procedure3(n, writer);

        writer.close();
    }

    private static void procedure1(int n, FileWriter writer) throws IOException {
        if (n > 0) {
            outputString(Integer.toString(n), writer);
            procedure1(n - 1, writer);
        };
    }

    private static void procedure2(int n, FileWriter writer) throws IOException {
        if (n > 0) {
            procedure2(n - 1, writer);
            outputString(Integer.toString(n), writer);
        }
    }

    private static void procedure3(int n, FileWriter writer) throws IOException {
        if (n > 0) {
            outputString(Integer.toString(n), writer);
            procedure3(n - 1, writer);
            outputString(Integer.toString(n), writer);
        }
    }

    private static void outputString(String n, FileWriter writer) throws IOException {
        if (writer == null) {
            System.out.println(n);
        } else {
            String strN = String.format("%s\n", n);
            writer.append(strN);
        }
    }

}
