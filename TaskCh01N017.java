/**
 * Записать по правилам изучаемого языка программирования следующие выражения:
 * о, п, р, с
 *
 *
 */

public class TaskCh01N017 {

    public static void main(String[] args) {
        // o)
        double x1 = Math.PI / 3;
        double y1 = Math.sqrt(1 - Math.pow(Math.sin(x1), 2));
        System.out.printf("%.4f\n", y1);
        // p)
        double a = 1, b = 2, c = 3;
        double x2 = 4;
        double y2 = 1 / Math.sqrt(a * Math.pow(x2, 2) + b * x2 + c);
        System.out.printf("%.5f\n", y2);
        // r)
        double x3 = 3;
        double y3 = (Math.sqrt(x3 + 1) + Math.sqrt(x3 - 1)) / (2 * Math.sqrt(x3));
        System.out.printf("%.5f\n", y3);
        // s)
        double x4 = -3;
        double y4 = Math.abs(x4) + Math.abs(x4 + 1);
        System.out.printf("%.0f", y4);
    }

}
