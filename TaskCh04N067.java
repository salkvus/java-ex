/**
 * Дано целое число k (1 <= k <= 365). Определить, каким будет k-й день года: вы-
 * ходным (суббота и воскресенье) или рабочим, если 1 января — понедельник.
 *
 */

import java.util.Scanner;

public class TaskCh04N067 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        boolean typeOfDay = isHoliday(k);
        System.out.printf("%d день года %s\n", k, typeOfDay ? "выходной" : "рабочий");
    }

    private static boolean isHoliday(int k) {
        return k % 7 == 6 || k % 7 == 0;
    }

}
