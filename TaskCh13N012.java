/**
 * Известна информация о 20 сотрудниках фирмы: фамилия, имя, отчество, адрес и дата поступления на работу
 * (месяц, год). Напечатать фамилию, имя, отчество и адрес сотрудников, которые на сегодняшний день
 * проработали в фирме не менее трех лет. День месяца не учитывать (при совпадении месяца поступления и
 * месяца сегодняшнего дня считать, что прошел полный год).
 *
 */

import java.time.LocalDate;
import java.util.ArrayList;

public class TaskCh13N012 {

    public static void main(String[] args) {
        Database employeesDatabase = createEmployeesDatabase();
        ArrayList<Employee> employees = employeesDatabase.findEmployees(3);
        for (var employee : employees) {
            System.out.println(employee);
        }
    }

    public static Database createEmployeesDatabase() {
        Database employees = new Database();
        employees.addRecord(new Employee("Иванов", "Иван", "Иванович",
                "г.Москва, ул.Зеленая, д.10, кв.8", 10, 2001));
        employees.addRecord(new Employee("Петров", "Сергей",
                "г.Москва, ул.Промышленная, д.15, кв.222", 5, 2010));
        employees.addRecord(new Employee("Васильев", "Сергей", "Иванович",
                "г.Москва, ул.Восточная, д.218, кв.127", 7, 2019));
        employees.addRecord(new Employee("Буласова", "Элина", "Радиковна",
                "г.Москва, ул.Береговая, д.19", 5, 2017));
        employees.addRecord(new Employee("Чураев", "Петр", "Константинович",
                "г.Москва, ул.Ленина, д.133, кв.315", 4, 2017));
        return employees;
    }

}

class Employee {
    private String lastName;
    private String name;
    private String middleName;
    private String address;
    private int month;

    public String getLastName() {
        return lastName;
    }

    public String getName() {
        return name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getAddress() {
        return address;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    private int year;

    public Employee() {
        this.lastName = "";
        this.name = "";
        this.middleName = "";
        this.address = "";
        this.month = 0;
        this.year = 0;
    }

    public Employee(String lastName, String name, String address, int month, int year) {
        this();
        this.lastName = lastName;
        this.name = name;
        this.address = address;
        this.month = month;
        this.year = year;
    }

    public Employee(String lastName, String name, String middleName, String address, int month, int year) {
        this(lastName, name, address, month, year);
        this.middleName = middleName;
    }

    @Override
    public String toString() {
        return String.join(", ","Стаж (в годах): " + getCurrentWorkingExperience(),  "ФИО: " + getFullName(), "Адрес: " + address);
    }

    public int getWorkingExperienceOnDate(int month, int year) {
        return year - this.year - (this.month < month ? 1 : 0);
    }

    public int getCurrentWorkingExperience() {
        LocalDate now = LocalDate.now();
        return getWorkingExperienceOnDate(now.getMonthValue(), now.getYear());
    }

    public String getFullName() {
        if (middleName.isBlank()) {
            return String.join(" ", name, lastName);
        }
        return String.join(" ", name, middleName, lastName);
    }
}

class Database {
    private ArrayList<Employee> employees;

    public Database() {
        this.employees = new ArrayList<>();
    }

    public void addRecord(Employee employee) {
        employees.add(employee);
    }

    public ArrayList<Employee> findEmployees(String fullNameSubstring) {
        ArrayList<Employee> result = new ArrayList<>();
        for (var item : employees) {
            if (item.getFullName().toLowerCase().indexOf(fullNameSubstring.toLowerCase()) != -1) {
                result.add(item);
            }
        }
        return result;
    }

    public ArrayList<Employee> findEmployees(int workingYears) {
        ArrayList<Employee> result = new ArrayList<>();
        for (var item : employees) {
            if (item.getCurrentWorkingExperience() >= workingYears) {
                result.add(item);
            }
        }
        return result;
//        return employees
//                .stream()
//                .filter(item -> item.getCurrentWorkingExperience >= workingYears)
//                .collect(Collectors.toCollection(ArrayList::new));
    }
}