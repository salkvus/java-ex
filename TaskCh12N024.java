/**
 * Заполнить массив размером 6 x 6 так, как показано на рис. 12.2.
 *
 * а) 1 1  1  1   1   1   б) 1 2 3 4 5 6
 *    1 2  3  4   5   6      2 3 4 5 6 1
 *    1 3  6 10  15  21      3 4 5 6 1 2
 *    1 4 10 20  35  56      4 5 6 1 2 3
 *    1 5 15 35  70 126      5 6 1 2 3 4
 *    1 6 21 56 126 252      6 1 2 3 4 5
 *
 */

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh12N024 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] arrayA = getArrayA(n);
        printArray(arrayA);
        System.out.println();
        int[][] arrayB = getArrayB(n);
        printArray(arrayB);
    }

    private static int[][] getArrayA(int n) {
        int[][] array = new int[n][n];
        for (int i = 0; i < array.length; ++i) {
            array[0][i] = 1;
            array[i][0] = 1;
        }
        for (int i = 1; i < array.length; ++i) {
            for (int j = i; j < array.length; ++j) {
                if (j == i) {
                    array[i][j] = 2 * array[i - 1][j];
                } else {
                    array[i][j] = array[i][j - 1] + array[i - 1][j];
                    array[j][i] = array[i][j];
                }
            }
        }
        return array;
    }

    private static int[][] getArrayB(int length) {
        int[][] array = new int[length][length];
        for (int i = 0; i < length; ++i) {
            for(int j = 0; j < length; ++j) {
                array[i][j] = i + j + 1 <= length ? i + j + 1 : (i + j + 1) - length;
            }
        }
//        for (int i = 1; i < array.length; ++i) {
//            array[i][array.length - 1] = array[i - 1][0];
//            System.arraycopy(array[i - 1], 1, array[i], 0, array.length - 1);
//        }
        return array;
    }

    private static void printArray(int[][] array) {
        for (var row : array) {
            System.out.println(Arrays.toString(row).replaceAll("[\\[\\],]", ""));
        }
    }
}
