/**
 * Написать рекурсивную функцию для вычисления индекса максимального элемента массива из n элементов.
 *
 */

import java.util.Scanner;

public class TaskCh10N049 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; ++i) {
            array[i] = scanner.nextInt();
        }
        int[] maxArray = {array[n - 1], n - 1};
        int max = getMax(array, maxArray, n - 2);
        System.out.println(max);
    }

    private static int getMax(int[] array, int[] maxArray, int k) {
        if (array[k] > maxArray[0]) {
            maxArray[0] = array[k];
            maxArray[1] = k;
        }
        if (k == 0) {
            return maxArray[1];
        };
        return getMax(array, maxArray, k - 1);
    }
}
