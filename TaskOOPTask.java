import java.util.ArrayList;

public class TaskOOPTask {

    public static void main(String[] args) {
        ArrayList<Candidate> candidates = createCandidates();
        Employer employer = new Employer("Julia");
        for(var candidate : candidates) {
            employer.hello();
            candidate.hello();
            candidate.describeExperience();
        }
    }

    private static ArrayList<Candidate> createCandidates() {
        ArrayList<Candidate> candidates = new ArrayList<>();
        candidates.add(new SelfLearner("Alexander"));
        candidates.add(new GetJavaJobGraduated("Dmitry"));
        return candidates;
    }
}

interface SaingHello { //abstraction
    void hello();
}

abstract class Person implements SaingHello {

    private String name; //encapsulation

    public Person() {
        this.name = "";
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() { //encapsulation
        return name;
    }
}

class Employer extends Person { //inheritance

    Employer(String name) {
        super(name);
    }

    @Override
    public void hello() {
        System.out.println("Hi! Introduce yourself and describe your java experience, please.");
    }
}

abstract class Candidate extends Person { //abastraction

    Candidate(String name) {
        super(name);
    }

    abstract public void describeExperience();

    @Override
    public void hello() {
        System.out.printf("Hi! My name is %s!\n", getName());
    }
}

class SelfLearner extends Candidate {  //inheritance

    SelfLearner(String name) {
        super(name);
    }

    @Override
    public void describeExperience() { //overriding (polymorphism)
        System.out.println("I've been learning Java by myself, nobody examined me how thorough is my knowledge" +
                " and how good is my code.");
    }
}

class GetJavaJobGraduated extends Candidate {  // inheritance

    GetJavaJobGraduated(String name) {
        super(name);
    }

    @Override
    public void describeExperience() { //overriding (polymorphism)
        System.out.println("I've passed successfully getJavaJob exams and code reviews.");
    }
}
