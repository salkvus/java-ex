/**
 * Написать рекурсивную функцию, определяющую, является ли заданное натуральное число простым
 * (простым называется натуральное число, большее 1, не имеющее других делителей, кроме единицы и самого себя).
 *
 */

import java.util.Scanner;

public class TaskCh10N056 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        boolean simple = isSimple(n);
        System.out.println(simple);
    }

    private static boolean isSimple(int n) {
        return helper(n, 2, n / 2);
    }

    private static boolean helper(int n, int divider, int bound) {
       if (divider > bound || divider == n) {
            return true;
        }
        return n % divider == 0 ? false : helper(n, divider + 1, n / (divider + 1));
    }

}
