/**
 * Даны два целых числа a и b.
 * Если a делится на b или b делится на a, то вывести 1, иначе — любое другое число.
 * Условные операторы и операторы цикла не использовать.
 *
 */

import java.util.Scanner;

public class TaskCh02N043 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        // 1
        int x = a % b;
        x = x % a + 1;
        // 2
        x = b / x % a;
        x = x % b + 1;
        System.out.println(x);
    }

}
