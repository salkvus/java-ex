import java.util.Scanner;

/**
 * Напечатать таблицу перевода 1, 2, ... 20 долларов США в рубли по текущему
 * курсу (значение курса вводится с клавиатуры).
 *
 */

public class TaskCh05N010 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float rate = scanner.nextFloat();
        printConversionTable(rate);
    }

    private static void printConversionTable(float rate) {
        for (int i = 1; i <= 20; ++i) {
            System.out.printf("$%d %.4f руб.\n", i, rate * i);
        }
    }

}
