/**
 * Даны целые числа h, m, s (0 < h ≤ 23, 0 ≤ m ≤ 59, 0 ≤ s ≤ 59), указывающие  момент времени:
 * "h часов, m минут, s секунд". Определить угол (в градусах) между положением часовой стрелки
 * в начале суток и в указанный момент времени.
 *
 */

import java.util.Scanner;

public class TaskCh02N039 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int h = scanner.nextInt();
        int m = scanner.nextInt();
        int s = scanner.nextInt();
        double angel = getAngle(h, m, s);
        System.out.println(angel);
    }

    private static double getAngle(int h, int m, int s) {
        double dH = 360 / 12;
        return (h % 12) *dH + m * dH / 60 + s * dH / 3600;
    }

}

