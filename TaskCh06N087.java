import java.util.Scanner;

/**
 *  Составить программу, которая ведет учет очков, набранных каждой командой при игре в баскетбол.
 *  Количество очков, полученных командами в ходе игры, может быть равно 1, 2 или 3. После любого
 *  изменения счет выводить на  экран. После окончания игры выдать итоговое сообщение и указать
 *  номер  команды-победительницы. Окончание игры условно моделировать вводом количества очков,
 *  равного нулю.
 *
 */

public class TaskCh06N087 {

    public static void main(String[] args) {
        Game game = new Game();
        game.inputTeam(1);
        game.inputTeam(2);
        game.play();
        game.showResult();
    }
}

class GameView {

    public void promptInputTeam(int team) {
        System.out.printf("Enter team #%d: ", team);
    }

    public void promptInputTeamToScore() {
        System.out.print("Enter team to score (1 or 2 or 0 to finish game): ");
    }

    public void promptInputScore() {
        System.out.print("Enter score (1 or 2 or 3): ");
    }
}

class Game {

    private Scanner input;

    private GameModel model;
    private GameView view;

    public Game() {
        this.model = new GameModel();
        this.view = new GameView();
        this.input = new Scanner(System.in);
    }

    public void play() {
        int team = inputTeamToScore();
        while (team != 0){
            int score = inputScore();
            model.addScore(team, score);
            showScore();
            team = inputTeamToScore();
        }
    }

    public void showResult() {
        System.out.println(result());
    }

    public String result() {
        String winner = model.getWinner();
        int winnerScore = model.getWinnerScore();
        String loser = model.getLoser();
        int loserScore = model.getLoserScore();
        return String.format("Final score %d : %d. The winner - %s, loser - %s", winnerScore, loserScore, winner, loser);
    }

    public void inputTeam(int team) {
        view.promptInputTeam(team);
        model.setTeam(team, input.nextLine());
    }

    public int inputTeamToScore() {
        view.promptInputTeamToScore();
        return input.nextInt();
    }

    public int inputScore() {
        view.promptInputScore();
        return input.nextInt();
    }

    public void showScore() {
        System.out.println(score());
    }

    public String score() {
        String team1 = model.getTeam(1);
        int score1 = model.getScore(1);
        String team2 = model.getTeam(2);
        int score2 = model.getScore(2);
        return String.format("Score %s %d : %s %d", team1, score1, team2, score2);
    }

}

class GameModel {

    private int score1;
    private String team1;
    private int score2;
    private String team2;

    public GameModel() {
        this.score1 = 0;
        this.team1 = "";
        this.score2 = 0;
        this.team2 = "";
    }

    public String getTeam(int team) {
        if (team == 1) {
            return team1;
        } else {
            return team2;
        }
    }

    public void setTeam(int team, String teamName) {
        if (team == 1) {
            this.team1 = teamName;
        } else {
            this.team2 = teamName;
        }
    }

    public int getScore(int team) {
        if (team == 1) {
            return score1;
        } else {
            return score2;
        }
    }

    public void addScore(int team, int score) {
        if (team == 1) {
            this.score1 += score;
        } else {
            this.score2 += score;
        }
    }

    public String getWinner() {
        return score1 > score2 ? team1 : team2;
    }

    public int getWinnerScore() {
        return score1 > score2 ? score1 : score2;
    }

    public String getLoser() {
        return score1 > score2 ? team2 : team1;
    }

    public int getLoserScore() {
        return score1 > score2 ? score2 : score1;
    }
}

