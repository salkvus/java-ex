/**
 * Написать рекурсивную функцию для вычисления значения так называемой функции Аккермана
 * для неотрицательных чисел n и m. Функция Аккермана определяется следующим образом:
 * 1) A(n, m) = m + 1, если n == 0
 * 2) A(n, m) = A(n - 1, 1), если n > 0, m == 0
 * 3) A(n, m) = A(n - 1, A(n, m - 1)), если n > 0, m > 0
 *
 * Функцию Аккермана называют дважды рекурсивной, т. к. сама функция и один из ее аргументов
 * определены через самих себя. Найти значение функции Аккермана для 1, 3. nm
 * Примечание Расчет значения функции Аккермана является трудоемким даже при малых аргументах n и m
 * (проверьте это утверждение для n = 4, m = 2).
 *
 */

import java.math.BigInteger;
import java.util.Scanner;

public class TaskCh10N050 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        BigInteger nAcker = ackermann(n, BigInteger.valueOf(m));
        System.out.println(nAcker);
    }

    private static BigInteger ackermann(int n, BigInteger m) {
        if (n == 0) {
            return m.add(BigInteger.valueOf(1));
        } else if (m.equals(BigInteger.valueOf(0))) {
            return ackermann(n - 1, BigInteger.valueOf(1));
        }
        return ackermann(n - 1, ackermann(n, m.subtract(BigInteger.valueOf(1))));
    }
}
