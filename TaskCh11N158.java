/**
 * Удалить из массива все повторяющиеся элементы, оставив их первые вхождения,
 * т. е. в массиве должны остаться только различные элементы.
 *
 */

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh11N158 {

    public static void main(String[] args) {
        int[] array = inputArray();
        printArray(array);
        transformArray(array);
        printArray(array);
    }

    private static void transformArray(int[] array) {
        int[] indexArray = new int[array.length];
        Arrays.fill(indexArray, 0, indexArray.length, -1);
        for (int i = 0; i < array.length; ++i) {
             checkArrayForDoubles(i, array, indexArray);
        }
        deleteItemsByIndex(array, indexArray);
    }

    private static void checkArrayForDoubles(int valueIndex, int[] array, int[] index) {
        int value = array[valueIndex];
        for (int i = valueIndex + 1; i < array.length; ++i) {
            if (array[i] == value) {
                if (index[i] != -1) {
                    break;
                }
                index[i] = valueIndex;
            }
        }
    }

    private static void deleteItemsByIndex(int[] array, int[] indexArray) {
        int shift = 0;
        int rightIndex = 0;
        for (int i = 0; i < indexArray.length; ++i) {
            if (indexArray[i] == -1) {
                continue;
            }
            rightIndex = getNextIndex(i, indexArray);
            if (rightIndex < array.length) {
                System.arraycopy(array, rightIndex - shift, array, i - shift, array.length - rightIndex + shift);
            }
            shift += rightIndex - i;
            i = rightIndex;
//            System.arraycopy(array, i + 1 - shift, array, i - shift, array.length - i - 1);
//            ++shift;
        }
        for (int i = 0; i < shift; ++i) {
            array[array.length - 1 - i] = 0;
        }
    }

    private static int getNextIndex(int rightBound, int[] indexArray) {
        for(; rightBound < indexArray.length && indexArray[rightBound] != -1; ++rightBound);
        return rightBound;
    }

    private static int[] inputArray() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; ++i) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

    private static void printArray(int[] array) {
        System.out.println(Arrays.toString(array).replaceAll("[\\[\\],]", ""));
    }
}
