/**
 * Заполнить двумерный массив размером 55 так, как представлено на рис. 12.4.
 *
 *  1  2  3  4 5
 * 16 17 18 19 6
 * 15 24 25 20 7
 * 14 23 22 21 8
 * 13 12 11 10 9
 *
 */

import java.util.Arrays;

public class TaskCh12N028 {

    public static void main(String[] args) {
        int n = 5;
        int[][] array = createArray(n);
        printArray(array);
    }

    private static int[][] createArray(int n) {
        int[][] array = new int[n][n];
        int value = 0;
        int iter = 0;
        do {
            value = fillRow(array, iter, n - 1 - iter, iter, value, true);
            value = fillColumn(array, iter + 1, n - 1 - iter, n - 1 - iter, value, true);
            value = fillRow(array, n - 1 - iter - 1, iter, n - 1 - iter, value, false);
            value = fillColumn(array, n - 1 - iter - 1, iter + 1, iter, value, false);
            iter++;
        } while (iter < (n + 1) / 2);
        return array;
    }

    private static int fillRow(int[][] array, int begin, int end, int rowIndex, int value, boolean leftToRight) {
        if (begin > end && leftToRight || end > begin && !leftToRight) {
            return value;
        }
        if (leftToRight) {
            for (int j = 0; j <= end - begin; ++j) {
                array[rowIndex][begin + j] = j + 1 + value;
            }
       } else {
            for (int j = begin - end; j >= 0; --j) {
                array[rowIndex][j + end] =  begin - end - j + 1 + value;
            }
        }
        return array[rowIndex][end];
    }

    private static int fillColumn(int[][] array, int begin, int end, int columnIndex, int value, boolean topToBottom) {
        if (begin > end && topToBottom || end > begin && !topToBottom) {
            return  value;
        }
        if (topToBottom) {
            for (int i = 0; i <= end - begin; ++i) {
                array[begin + i][columnIndex] = i + 1 + value;
            }
        } else {
            for (int i = begin - end; i >= 0; --i) {
                array[i + end][columnIndex] = begin - end - i + 1 + value;
            }
        }
        return array[end][columnIndex];
    }


    private static void printArray(int[][] array) {
        for (var row : array) {
            System.out.println(Arrays.toString(row).replaceAll("[\\[\\],]", ""));
        }
    }
}
