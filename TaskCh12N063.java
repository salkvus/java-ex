import java.util.Arrays;
import java.util.Scanner;

public class TaskCh12N063 {

    public static void main(String[] args) {
        final int n = 3;
        final int m = 4;
        int[][] array = inputArray(n, m);
        int[] avgArray = getAverageFromArray(array);
        printArray(avgArray);
    }

    private static int[] getAverageFromArray(int[][] array) {
        int[] avgArray = new int[array.length];
        for (int i = 0; i < array.length; ++i) {
            int sum = 0;
            for (int j = 0; j < array[i].length; ++j) {
                sum += array[i][j];
            }
            avgArray[i] = sum / array[i].length;
        }
        return avgArray;
    }

    private static int[][] inputArray(int n, int m) {
        Scanner scanner = new Scanner(System.in);
        int[][] array = new int[n][m];
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                array[i][j] = scanner.nextInt();
            }
        }
        return array;
    }

    private static void printArray(int[] array) {
        System.out.println(Arrays.toString(array).replaceAll("[\\[\\],]", ""));
    }
}
