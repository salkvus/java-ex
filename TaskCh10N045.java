/**
 *  Даны первый член и разность арифметической прогрессии. Написать рекурсивную функцию для нахождения:
 *  а) n-го члена прогрессии;
 *  б) суммы n первых членов прогрессии.
 *
 */

import java.util.Scanner;

public class TaskCh10N045 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long a1 = scanner.nextLong();
        int d = scanner.nextInt();
        long n = scanner.nextLong();
        long aN = getNthMember(a1, d, n);
        long sum = getProgressionSum(a1, d, n);
        System.out.println(aN);
        System.out.println(sum);
    }

    private static long getNthMember(long a1, int d, long n) {
        if (n == 1) {
            return a1;
        }
        return d + getNthMember(a1, d, n - 1);
    }

    private static long getProgressionSum(long a, int d, long n) {
        if (n == 0) {
            return 0;
        }
        return a + getProgressionSum(a + d, d, n - 1);
    }
}
