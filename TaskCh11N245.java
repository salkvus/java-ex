/**
 * Дан массив. Переписать его элементы в другой массив такого же размера следующим образом:
 * сначала должны идти все отрицательные элементы,  а затем все остальные.
 * Использовать только один проход по исходному массиву.
 **
 */

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh11N245 {

    public static void main(String[] args) {
        int[] srcArray = inputArray();
        printArray(srcArray);
        int[] destArray = new int[srcArray.length];
        transformArray(srcArray, destArray);
        printArray(destArray);
    }

    private static void transformArray(int[] srcArray, int[] dstArray) {
        for (int i = 0, j = srcArray.length - 1; i < srcArray.length && i <= j; ++i) {
            if (srcArray[i] < 0) {
                dstArray[i] = srcArray[i];
            } else {
                while (i <= j) {
                    if (srcArray[j] < 0) {
                        int tmp = srcArray[i];
                        dstArray[i] = srcArray[j];
                        dstArray[j] = tmp;
                        --j;
                        break;
                    } else {
                        dstArray[j] = srcArray[j];
                    }
                    --j;
                }
            }
        }
    }

    private static int[] inputArray() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; ++i) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

    private static void printArray(int[] array) {
        System.out.println(Arrays.toString(array).replaceAll("[\\[\\],]", ""));
    }
}
