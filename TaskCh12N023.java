/**
 *  так, как показано на рис. 12.1.
 *
 * а) 1 0 0 0 0 0 1     б) 1 0 0 1 0 0 1
 *    0 1 0 0 0 1 0        0 1 0 1 0 1 0
 *    0 0 1 0 1 0 0        0 0 1 1 1 0 0
 *    0 0 0 1 0 0 0        1 1 1 1 1 1 1
 *    0 0 1 0 1 0 0        0 0 1 1 1 0 0
 *    0 1 0 0 0 1 0        0 1 0 1 0 1 0
 *    1 0 0 0 0 0 1        1 0 0 1 0 0 1
 *
 * в) 1 1 1 1 1 1 1
 *    0 1 1 1 1 1 0
 *    0 0 1 1 1 0 0
 *    0 0 0 1 0 0 0
 *    0 0 1 1 1 0 0
 *    0 1 1 1 1 1 0
 *    1 1 1 1 1 1 1
 *
 */

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh12N023 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] arrayA = getArrayA(n);
        printArray(arrayA);
        System.out.println();
        int[][] arrayB = getArrayB(n);
        printArray(arrayB);
        System.out.println();
        int[][] arrayC = getArrayC(n);
        printArray(arrayC);
    }

    private static int[][] getArrayA(int n) {
        int[][] array = new int[n][n];
        for (int i = 0; i <= array.length / 2; ++i) {
            array[i][i] = 1;
            array[i][array.length - i - 1] = 1;
        }
        for (int i = array.length - 1; i > array.length / 2; --i) {
            System.arraycopy(array[array.length - 1 - i], 0, array[i], 0, array.length);
        }
        return array;
    }

    private static int[][] getArrayB(int n) {
        int[][] array = new int[n][n];
        for (int i = 0; i < array.length / 2; ++i) {
            array[i][i] = 1;
            array[i][array.length / 2] = 1;
            array[i][array.length - i - 1] = 1;
        }
        for (int i = 0; i < array.length; ++i) {
            array[array.length / 2][i] = 1;
        }
        for (int i = array.length - 1; i > array.length / 2; --i) {
            System.arraycopy(array[array.length - 1 - i], 0, array[i], 0, array.length);
        }
        return array;
    }

    private static int[][] getArrayC(int n) {
        int[][] array = new int[n][n];
        for (int i = 0; i <= array.length / 2; ++i) {
            for (int j = i; j <= array.length / 2; ++j) {
                array[i][j] = 1;
            }
            System.arraycopy(array[i], i, array[i], array.length / 2 + 1, array.length / 2 - i);
        }
        for (int i = array.length - 1; i > array.length / 2; --i) {
            System.arraycopy(array[array.length - 1 - i], 0, array[i], 0, array.length);
        }
        return array;
    }

    private static void printArray(int[][] array) {
        for (var row : array) {
            System.out.println(Arrays.toString(row).replaceAll("[\\[\\],]", ""));
        }
    }
}
