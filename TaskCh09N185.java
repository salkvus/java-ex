/**
 * Строка содержит арифметическое выражение, в котором используются круглые скобки, в том числе вложенные.
 * Проверить, правильно ли в нем расставлены скобки.
 * а) Ответом должны служить слова да или нет.
 * б) В случае неправильности расстановки скобок:
 * если имеются лишние правые (закрывающие) скобки, то выдать сообщение с указанием позиции
 * первой такой скобки;
 * если имеются лишние левые (открывающие) скобки, то выдать сообщение с указанием количества таких скобок.
 * Если скобки расставлены правильно, то сообщить об этом.
 *
 */

import java.util.ArrayList;
import java.util.Scanner;

public class TaskCh09N185 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[] chars = scanner.nextLine().trim().toCharArray();
        Parser parser = new Parser();
        parser.analyzeParentheses(chars);
        if (parser.isValidState()) {
            System.out.println("Да.");
        } else {
            System.out.println("Нет.");
            System.out.println(parser.getErrorMessage());
        }
    }
}

class ParseError {

    private String message = "";

    public boolean status = false;

    public void setMessage(ParserStates parserState, int position) {
        switch (parserState) {
            case VALID:
                message = "";
                break;
            case ERROR_MORE_LEFT_PARENTHESIS:
                message = String.format("Лишние левые (открывающие) скобки: %d скобок", position);
                break;
            case ERROR_MORE_RIGHT_PARENTHESIS:
                message = String.format("Лишние правые (закрывающие) скобки: позиция скобки %d", position);
                break;
            default:
                message = "Неопределенная ошибка";
        }
    }

    public String getMessage() {
        return message;
    }
}

enum ParserStates {
    VALID, ERROR_MORE_LEFT_PARENTHESIS, ERROR_MORE_RIGHT_PARENTHESIS;
}

class Parser {

    private Stack charStack;
    private ParserStates state;
    private ParseError error;

    public Parser() {
        this.charStack = new Stack();
        this.state = ParserStates.VALID;
        this.error = new ParseError();
    }

    public boolean isValidState() {
        return state == ParserStates.VALID;
    }

    public String getErrorMessage() {
        return error.getMessage();
    }

    public void analyzeParentheses(char[] chars) {
        int index = -1;
        for (int i = 0; i < chars.length; ++i) {
            index = i;
            if (chars[i] == '(') {
                charStack.push(Character.valueOf(chars[i]));
            } else if (chars[i] == ')') {
                if (charStack.isEmpty()) {
                    setState(ParserStates.ERROR_MORE_RIGHT_PARENTHESIS, index + 1);
                    break;
                }
                charStack.pop();
            }
        }
        if (!charStack.isEmpty()) {
            setState(ParserStates.ERROR_MORE_LEFT_PARENTHESIS, charStack.getTop() + 1);
        }
    }

    private void setState(ParserStates state, int position) {
        this.state = state;
        error.status = !isValidState();
        error.setMessage(state, position);
    }
}

class Stack {

    private ArrayList<Character> array;
    private int top;

    public Stack() {
        this.array = new ArrayList<>(20);
        this.top = -1;
    }

    public void push(Character c) {
        array.add(c);
        ++top;
    }

    public Character pop() {
        Character c = null;
        if (top < 0) {
            return c;
        }
        c = array.get(top);
        array.remove(top);
        --top;
        return c;
    }

    public int getTop() {
        return this.top;
    }

    public boolean isEmpty() {
        return top < 0;
    }
}